(defsystem "saurna"
  :version "0.1.0"
  :author "Momozor"
  :license "MIT"
  :depends-on ("lispbuilder-sdl"
               "lispbuilder-sdl-gfx"
               "lispbuilder-sdl-ttf")
  :components ((:module "src"
                        :components
                        ((:file "main"))))
  :description "A Gomoku/Paper Chess style board game"
  :build-operation "program-op" ;; leave as is
  :build-pathname "saurna"
  :entry-point "saurna:main")
