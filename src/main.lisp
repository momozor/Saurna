;;;; Saurna - A board game that looks like Gomoku.
;;;;
;;;; Copyright (C) 2019 Momozor <skelic3@gmail.com>
;;;; For license, see LICENSE file for further details.

(in-package :cl-user)
(defpackage saurna
  (:use :cl)
  (:export :main))
(in-package :saurna)

;;; Constants.
(defparameter *background-color* (sdl:color
                                  :r 217
                                  :g 209
                                  :b 181))
(defparameter *data-root* (asdf:system-source-directory 'saurna))
(defparameter *font-root* (merge-pathnames "fonts/" *data-root*))
(defparameter *noto-sans*
  (make-instance 'sdl:ttf-font-definition
                 :size 24
                 :filename (merge-pathnames "NotoSans-Regular.ttf" *font-root*)))

(defparameter *screen-width* 411)
(defparameter *screen-height* 450)
(defparameter *total-vertical-lines* 41)
(defparameter *total-horizontal-lines* 26)
(defparameter *horizontal-coordinates* '())
(defparameter *vertical-coordinates* '())
(defparameter *pixels* 10)
(defparameter *steps* *pixels*)
(defparameter *vertical-lines-by-pixels* (* *pixels* *total-vertical-lines*))
(defparameter *horizontal-lines-by-pixels* (* *pixels* *total-horizontal-lines*))
(defparameter *reserved-coordinates* '())

;;; Structures.

(defstruct player
  name
  (score 0)
  color
  (last-coordinates '())
  (picked-coordinates '()))

;;; Structure instances.

(defparameter *you* (make-player
                     :name "You"
                     :color sdl:*blue*))

(defparameter *cpu* (make-player
                     :name "CPU"
                     :color sdl:*red*))

;;; Functions.

(defun random-choice (sequence)
  "Take a random element out of sequence."
  (let ((i (random (length sequence))))
    (nth i sequence)))

(defun gameboard-draw-vertical-lines ()
  "Draw the gameboard grid, for the vertical lines."
  (loop
     for i
     from 0 to *horizontal-lines-by-pixels* by *steps*
     do (progn
          (sdl:draw-line-* 0 i *vertical-lines-by-pixels* i)
          (push i *vertical-coordinates*)))
  (setf *vertical-coordinates* (remove-duplicates *vertical-coordinates*)))

(defun gameboard-draw-horizontal-lines ()
  "Draw the gameboard grid, for the horizontalgia lines."
  (loop
     for i
     from 0 to *vertical-lines-by-pixels* by *steps*      
     do (progn
          (sdl:draw-line-* i 0 i *horizontal-lines-by-pixels*)
          (push i *horizontal-coordinates*)))
  (setf *horizontal-coordinates* (remove-duplicates *horizontal-coordinates*)))

(defun draw-scoreboard-text ()
  "Draw scoreboard text indicators."
  (let* ((human-player-left-offset 340)
         (human-player-bottom-offset 140)
         (cpu-player-left-offset 100)
         (cpu-player-bottom-offset 140)
         (human-player-x-position (- *screen-width* human-player-left-offset))
         (human-player-y-position (- *screen-height* human-player-bottom-offset))
         (cpu-player-x-position (- *screen-width* cpu-player-left-offset))
         (cpu-player-y-position (- *screen-height* cpu-player-bottom-offset)))
    (sdl:draw-string-blended-* (player-name *you*)
                               human-player-x-position
                               human-player-y-position
                               :color (player-color *you*))
    (sdl:draw-string-blended-* (player-name *cpu*)
                               cpu-player-x-position
                               cpu-player-y-position 
                               :color (player-color *cpu*))))

;; TODO: Actually update the score states too.
(defun update-score ()
  "Update the score string indicators for each players."
  (let* ((human-player-left-offset 340)
         (human-player-bottom-offset 100)
         (cpu-player-left-offset 100)
         (cpu-player-bottom-offset 100)
         (human-player-x-position (- *screen-width* human-player-left-offset))
         (human-player-y-position (- *screen-height* human-player-bottom-offset))
         (cpu-player-x-position (- *screen-width* cpu-player-left-offset))
         (cpu-player-y-position (- *screen-height* cpu-player-bottom-offset)))
    (sdl:draw-string-blended-* (princ-to-string
                                (player-score *you*))
                               human-player-x-position
                               human-player-y-position
                               :color (player-color *you*))

    (sdl:draw-string-blended-* (princ-to-string
                                (player-score *cpu*))
                               cpu-player-x-position
                               cpu-player-y-position
                               :color (player-color *cpu*))))

(defun draw-player-turn-indicator-text () 
  "Just simply draw 'Turn' at the middle of game screen."
  (let* ((screen-left-offset 250)
         (screen-bottom-offset 80)
         (x-position (- *screen-width* screen-left-offset))
         (y-position (- *screen-height* screen-bottom-offset))) 
    (sdl:draw-string-blended-* "Turn"
                               x-position
                               y-position
                               :color sdl:*black*)))

(defun update-player-turn (player)
  "Re-draw or update the player turn string indicator."
  (let* ((screen-left-offset 190)
         (screen-bottom-offset 80)
         (x-position (- *screen-width* screen-left-offset))
         (y-position (- *screen-height* screen-bottom-offset)))
    (sdl:draw-string-blended-* (player-name player)
                               x-position
                               y-position
                               :color (player-color player))
    (sdl:update-display)))

(defun hide-previous-player-turn ()
  "Hack. Re-draw a circle on top of previous player turn drawn string."
  (let* ((screen-left-offset 160)
         (screen-bottom-offset 40)
         (x-position (- *screen-width* screen-left-offset))
         (y-position (- *screen-height* screen-bottom-offset))
         (max-radius 47))
    (sdl:draw-filled-circle-* x-position
                              y-position
                              max-radius
                              :color *background-color*))
  (sdl:update-display))

(defun within-gameboard-p (x y)
  "Checks if the coordinates pointed in the gameboard area." 
  (and (<= x (* *total-vertical-lines* *steps*))
       (<= y (* *total-horizontal-lines* *steps*))))

(defun at-line-intersection-p (x y)
  "Checks if the coordinates are between the even of x and y lines."
  (and
   (find x *horizontal-coordinates*)
   (find y *vertical-coordinates*)))

(defun coordinates-reserved-p (x y) 
  "Checks if the coordinates are reserved."
  (find-if
   #'(lambda (i)
       (and (equal (second i) x)
            (equal (fourth i) y)))
   *reserved-coordinates*))

(defun update-player-coordinates (player x y)
  "Update the player coordinates structure state."
  (push `(:x ,x :y ,y) *reserved-coordinates*)
  (push `(:x ,x :y ,y) (player-picked-coordinates player))
  (setf (player-last-coordinates player) (list x y)))

;; FIXME: Fix this logic.
(defun draw-connected-lines (player-coordinates color)
  "Just draw the lines given how the dots."
  (loop
     for i
     in player-coordinates
     do (progn
          (sdl:draw-line-* (second i) (fourth i) (fourth i) (second i) :color color)))
  (sdl:update-display))

;; FIXME: Fix the logic.
(defun connect-dots (player) 
  "Connect the dots with a line."
  (let ((player-coordinates (player-picked-coordinates player))
        (color (player-color player)))
    (when (equal (length player-coordinates) 2)
      (draw-connected-lines player-coordinates color))))

(defun draw-dot (player x y)
  "Draw dot on the grid according given the coordinates."
  (let ((max-radius 4))
    (sdl:draw-filled-circle-* x y max-radius :color (player-color player))
    (update-player-coordinates player x y)
    (sdl:update-display)))

(defun near-human-dot-p (cpu-coordinates human-coordinates)
  "Is CPU dot is near the human's dot?"
  (let ((human-x-coordinate (first human-coordinates))
        (human-y-coordinate (second human-coordinates))
        (cpu-x-coordinate (first cpu-coordinates))
        (cpu-y-coordinate (second cpu-coordinates)))
    (and (< cpu-x-coordinate human-x-coordinate)
         (< cpu-y-coordinate human-y-coordinate))))

(defun ai-moves ()
  "Handle AI/CPU player for the automatic moves."
  (let ((random-x (random-choice *horizontal-coordinates*))
        (random-y (random-choice *vertical-coordinates*))
        (cpu-player-last-coordinates (player-last-coordinates *cpu*))
        (human-player-last-coordinates (player-last-coordinates *you*)))

    ;; Make sure random-x and random-y will ALWAYS
    ;; be unique and not reserved on the
    ;; gameboard.
    (loop
       for i in *reserved-coordinates*
         
       ;; Have to use i to silence unused i variable warning.
       while (and (equal random-x (second i))
                  (equal random-y (fourth i))

                  ;; Make sure the cpu place the dot next to
                  ;; or near the last human placed dot.
                  (near-human-dot-p cpu-player-last-coordinates
                                    human-player-last-coordinates))
       do (progn (setf random-x (random-choice *horizontal-coordinates*))
                 (setf random-y (random-choice *vertical-coordinates*))))

    (when (not (coordinates-reserved-p random-x random-y))
      (draw-dot *cpu* random-x random-y))))

(defun game-engine ()
  "All game specific logic resides in here."
  (let ((current-player-turn *you*))
    (draw-player-turn-indicator-text)
    (draw-scoreboard-text)
    (update-score)
    (update-player-turn current-player-turn)
    
    (sdl:update-display)

    (sdl:with-events ()
      (:quit-event () t)
      (:key-down-event (:key key)
                       (when (or (sdl:key= key :SDL-KEY-Q)
                                 (sdl:key= key :SDL-KEY-ESCAPE))
                         (sdl:push-quit-event)))
      (:video-expose-event () (sdl:update-display)) 
      (:mouse-button-down-event (:x x :y y)
                                (when (and (at-line-intersection-p x y)
                                           (within-gameboard-p x y)
                                           (not (coordinates-reserved-p x y))
                                           (equal current-player-turn *you*))
                                  (draw-dot *you* x y)
                                  (setf current-player-turn *cpu*)
                                  (hide-previous-player-turn)
                                  (update-player-turn current-player-turn)))
      (:mouse-button-up-event (:x x :y y) 
                              (when (and (within-gameboard-p x y)
                                         (equal current-player-turn *cpu*))
                                (sleep 1) ; Give an illusion that cpu is making for a move.
                                (setf current-player-turn *you*)
                                (ai-moves)
                                (hide-previous-player-turn)
                                (update-player-turn current-player-turn))))))

(defun game-init ()
  "Initialize all the assets and objects."
  (let ((max-fps 60))
    (sdl:with-init ()
      (sdl:window *screen-width* *screen-height* :title-caption "Saurna")
      (setf (sdl:frame-rate) max-fps) 
      (sdl:clear-display *background-color*)

      (unless (sdl:initialise-default-font *noto-sans*)
        (error "SAURNA: Cannot initialize the default Noto+Sans font."))

      (sdl:with-surface (surf sdl:*default-display*)
        (sdl:with-color (col (sdl:color :r 0 :g 0 :b 163))
          (gameboard-draw-horizontal-lines)
          (gameboard-draw-vertical-lines)) 
        (game-engine)))))

(defun main ()
  "Game execution point."
  (game-init))
