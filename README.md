# Saurna
It looks like Gomoku, but no. Actually, it is https://en.wikipedia.org/wiki/Dots_(game)".
Inspired by https://github.com/teohhanhui/paper-chess.

![saurna](https://github.com/momozor/Saurna/blob/master/screenshots/saurna-1.png)

## Installation & Play
```
$ git clone https://github.com/momozor/saurna
$ sbcl --eval '(ql:quickload :saurna)' --eval '(saurna:main)'

### or

$ git clone https://github.com/momozor/saurna
$ make LISP=sbcl
$ ./saurna
```

## Font License
* NotoSans-Regular.ttf
  - Copyright 2012 Google Inc. All Rights Reserved. under the Apache 2.0 license.
  - Please see README and LICENSE files in fonts/ directory folder for more details.

# License
This project is released under the MIT license. Pleasee see LICENSE file
for further details.
